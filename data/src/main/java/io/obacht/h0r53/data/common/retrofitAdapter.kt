package io.obacht.h0r53.data.common

import arrow.Kind
import arrow.core.Either
import arrow.core.left
import arrow.core.right
import arrow.fx.typeclasses.Async
import arrow.fx.typeclasses.MonadDefer
import arrow.typeclasses.ApplicativeError
import arrow.typeclasses.MonadError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response


data class CallK<R>(val call: Call<R>) {
    fun <F> async(AC: Async<F>): Kind<F, Response<R>> = call.runAsync(AC)

    fun <F> defer(defer: MonadDefer<F>): Kind<F, Response<R>> = call.runSyncDeferred(defer)

    fun <F> catch(monadError: MonadError<F, Throwable>): Kind<F, Response<R>> = call.runSyncCatch(monadError)
}


fun <F, R> Response<R>.unwrapBody(apError: ApplicativeError<F, Throwable>): Kind<F, R> =
    if (this.isSuccessful) {
        val body = this.body()
        if (body != null) {
            apError.just(body)
        } else {
            apError.raiseError(IllegalStateException("The request returned a null body"))
        }
    } else {
        apError.raiseError(HttpException(this))
    }

fun <F, A> Call<A>.runAsync(AC: Async<F>): Kind<F, Response<A>> =
    AC.async { callback ->
        enqueue(ResponseCallback(callback))
    }

fun <F, A> Call<A>.runSyncDeferred(defer: MonadDefer<F>): Kind<F, Response<A>> = defer.later { execute() }

fun <F, A> Call<A>.runSyncCatch(monadError: MonadError<F, Throwable>): Kind<F, Response<A>> =
    monadError.run {
        catch {
            execute()
        }
    }


class ResponseCallback<R>(private val proc: (Either<Throwable, Response<R>>) -> Unit) :
    Callback<R> {
    override fun onResponse(call: Call<R>, response: Response<R>) {
        proc(response.right())
    }

    override fun onFailure(call: Call<R>, throwable: Throwable) {
        proc(throwable.left())
    }
}
