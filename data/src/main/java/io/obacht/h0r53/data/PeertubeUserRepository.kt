package io.obacht.h0r53.data

import io.obacht.h0r53.data.source.ApiClient
import io.obacht.h0r53.domain.entities.ClientToken
import io.obacht.h0r53.domain.user.UserRepository
import io.reactivex.Single

//
//class PeertubeUserRepository(private val apiClient: ApiClient) :
//    UserRepository {
//
//    override fun connect(): IO<ClientToken> =
//            apiClient.getClientToken()
//                .async(IO.async())
//                .flatMap { response -> response.unwrapBody(IO.applicativeError()) }
//}

class PeertubeUserRepository(
    private val apiClient: ApiClient
) : UserRepository {

    override fun connect(): Single<ClientToken> =
        apiClient.getClientToken()



//    override fun isFavorite(photoId: Long): Boolean {
//        val loadOneByPhotoId = database.photoDao.loadOneByPhotoId(photoId)
//        return loadOneByPhotoId != null
//    }
//
//    override fun deletePhoto(photo: Photo) {
//        database.photoDao.delete(photo)
//    }
//
//    override fun addPhoto(photo: Photo) {
//        database.photoDao.insert(photo)
//    }
//
//
//    override fun getPhotoDetail(photoId: Long?): Single<Photo> {
//        return apiClient.getPhotoDetail(photoId!!)
//    }
//
//    override fun getPhotos(albumId: Long?): Single<List<Photo>> {
//        return apiClient.getPhotos(albumId!!)
//    }

}