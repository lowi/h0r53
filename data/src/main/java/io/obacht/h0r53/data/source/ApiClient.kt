package io.obacht.h0r53.data.source

import io.obacht.h0r53.domain.entities.ClientToken
import io.obacht.h0r53.domain.entities.Video
import io.obacht.h0r53.domain.entities.VideoList
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.GET

interface ApiClient {

//    @GET("oauth-clients/local")
    @GET("connect")
    fun getClientToken(): Single<ClientToken>

    @GET("videos")
    fun allVideos(): Single<VideoList>
}


