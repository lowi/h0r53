package io.obacht.h0r53.data.source

import io.obacht.h0r53.domain.entities.ClientToken
import io.obacht.h0r53.domain.entities.Video
import io.obacht.h0r53.domain.services.ConnectionService
import io.reactivex.Flowable
import io.reactivex.Single

class ConnectionServiceImpl(private val apiClient: ApiClient) : ConnectionService {
    override fun connect(): Single<ClientToken> = apiClient.getClientToken()

    override fun videos(): Single<List<Video>> =
        // map the VideoList data object to a list of Video Data objects (List<Video>)
        apiClient.allVideos().map {
            it.data
        }
}