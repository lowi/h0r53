#!/bin/sh
openapi-generator generate \
    --generator-name kotlin \
    --api-package io.obacht.peertube.api \
    --artifact-id peertube-client \
    --package-name io.obacht.peertube.api \
    --group-id io.obacht.h0r53.data.peertube \
    --artifact-id io.obacht.peertube \
    --artifact-version 1.0.0 \
    --skip-validate-spec \
    --output out \
    --verbose \
    -i peertube_openapi.yaml
