package io.obacht.h0r53.presentation.platform


sealed class Resource<T>() {
    data class Success<T>(val data: T) : Resource<T>()
//    class Error<T>(val msg: String, val data: T) : Resource<T>()
    class Error<T>(val msg: String) : Resource<T>()
    class Loading<T>(val data: T) : Resource<T>()
}