package io.obacht.h0r53.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.obacht.h0r53.domain.entities.ClientToken
import io.obacht.h0r53.domain.usecases.ConnectUseCase
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable


class ClientTokenViewModel(private val connectUseCase: ConnectUseCase) : ViewModel() {

    val token = MutableLiveData<ClientToken>()

    fun connect() {
        connectUseCase
            .execute()
            .subscribe(
                object : SingleObserver<ClientToken> {

                    override fun onSuccess(token: ClientToken) {
                        println("connection successful: $token")
                        this@ClientTokenViewModel.token.value = token
                    }

                    override fun onError(e: Throwable) {
                        onUnexpectedError(e)
                    }

                    override fun onSubscribe(d: Disposable) {
                        println("onSubscribe. disposable: $d")
                    }

                }
            )
    }

    private fun onUnexpectedError(e: Throwable) {
        println(e.message)
    }

}
