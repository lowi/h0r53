package io.obacht.h0r53.presentation

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.multidex.MultiDex
import com.github.quentin7b.kointimber.TimberLogger
import io.obacht.h0r53.presentation.di.appModule
import io.obacht.h0r53.presentation.platform.FakeCrashLibrary
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber
import timber.log.Timber.DebugTree
import xyz.lowi.h0r53.BuildConfig


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        initLogging()
        loadKoin()
//        Stetho.initializeWithDefaults(this)
    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    private fun initLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

    private fun loadKoin() {
        startKoin {
//            TimberLogger()
            androidLogger()
            androidContext(this@App)
            androidFileProperties()
            modules(appModule)
        }
    }

    /** A tree which logs important information for crash reporting.  */
    private class CrashReportingTree : Timber.Tree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) return

            FakeCrashLibrary.log(priority, tag!!, message)

            if (t != null) {
                if (priority == Log.ERROR) {
                    FakeCrashLibrary.logError(t)
                } else if (priority == Log.WARN) {
                    FakeCrashLibrary.logWarning(t)
                }
            }
        }
    }
}