package io.obacht.h0r53.presentation.user.presentation
//
//import arrow.core.Either
//import io.obacht.h0r53.domain.entities.PeertubeError
//
//sealed class QueryViewState<out T> {
//
//    companion object {
//        fun <T> result(results: List<T>): QueryViewState<T> =
//            Result(results)
//        fun <T> idle(): QueryViewState<T> =
//            Idle
//        fun <T> loading(): QueryViewState<T> =
//            Loading
//        fun <T> error(error: PeertubeError): QueryViewState<T> =
//            Error(error)
//    }
//
//    object Idle : QueryViewState<Nothing>()
//    object Loading : QueryViewState<Nothing>()
//    data class Result<out T>(val _results: List<T>) : QueryViewState<T>()
//    data class Error(val _error: PeertubeError) : QueryViewState<Nothing>()
//
//}
//
//fun <T> Either<PeertubeError, List<T>>.toViewState(): QueryViewState<T> = fold(
//    { QueryViewState.error(it) },
//    { QueryViewState.result(it) }
//)
