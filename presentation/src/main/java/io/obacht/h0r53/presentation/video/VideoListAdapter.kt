package io.obacht.h0r53.presentation.video

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DiffUtil.calculateDiff
import io.obacht.h0r53.domain.entities.Video
import io.obacht.h0r53.presentation.platform.SingleLayoutAdapter
import xyz.lowi.h0r53.R


typealias ClickListener = (Video) -> Unit

class VideoListAdapter : SingleLayoutAdapter(R.layout.list_item_video) {

    private var videoList: List<Video> = emptyList()

    override fun getItemCount() = videoList.size

    override fun getObjForPosition(position: Int): Any = videoList[position]


    fun updateVideos(videoList: List<Video>) {
        val diffResult = calculateDiff(VideoDiffCallback(this.videoList, videoList))
        this.videoList = videoList
        diffResult.dispatchUpdatesTo(this)
    }


    override fun getSectionName(position: Int): String = videoList[position].uuid


    companion object {

        private val diffCallback = object : DiffUtil.ItemCallback<Video>() {
            override fun areItemsTheSame(
                oldItem: Video,
                newItem: Video
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: Video,
                newItem: Video
            ): Boolean =
                oldItem == newItem
        }
    }

}


class VideoDiffCallback(
    private val old: List<Video>,
    private val new: List<Video>
) : DiffUtil.Callback() {
    override fun getOldListSize() = old.size

    override fun getNewListSize() = new.size

    override fun areItemsTheSame(oldIndex: Int, newIndex: Int): Boolean {
        return old[oldIndex].id == new[newIndex].id
    }

    override fun areContentsTheSame(oldIndex: Int, newIndex: Int): Boolean {
        return old[oldIndex] == new[newIndex]
    }
}

//
//class VideoListAdapter(
//    private val layoutId: Int,
//    private val viewModel: VideoListViewModel
//) : RecyclerView.Adapter<VideoListAdapter.ViewHolder>() {
//
//    private val videos: List<Video> = emptyList()
//
//
//
////    init {
////        flowable.observeOn(Schedulers.computation())
////            .scan(data to data) { prev, next -> prev.second to next }
////            .skip(1)
////            .map { (old, new) -> new to diffResult(old, new) }
////            .observeOn(AndroidSchedulers.mainThread())
////            .doOnNext { Log.d("Diff", "notified adapter: $this") }
////            .subscribe { (names, changes) ->
////                data = names
////                changes.dispatchUpdatesTo(this)
////            }
////    }
//
//    override fun getItemCount() = videos.size
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val binding = DataBindingUtil
//            .inflate(from(parent.context), xyz.lowi.h0r53.R.layout.list_item_video, parent, false)
//        return ViewHolder(binding)
//    }
//
//    override fun onBindViewHolder(holder: GenericViewHolder, position: Int) {
//        holder.bind(viewModel, position)
//    }
//
////    override fun onBindViewHolder(holder: Holder, position: Int) = holder.bind(data[position])
//
//    private fun diffResultdiffResult(old: List<String>, new: List<String>): DiffUtil.DiffResult {
//        return DiffUtil.calculateDiff(object : DiffUtil.Callback() {
//            override fun getOldListSize() = old.size
//
//            override fun getNewListSize() = new.size
//
//            override fun areItemsTheSame(i: Int, j: Int) = old[i] == new[j]
//
//            override fun areContentsTheSame(i: Int, j: Int) = true
//        })
//    }
//
//    class ViewHolder(private val binding: ListItemVideoBinding) :
//        RecyclerView.ViewHolder(binding.root)
//}