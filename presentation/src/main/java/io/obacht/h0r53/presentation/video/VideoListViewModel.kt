package io.obacht.h0r53.presentation.video


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import io.obacht.h0r53.domain.entities.Video
import io.obacht.h0r53.domain.usecases.VideosUseCase
import io.obacht.h0r53.presentation.platform.BaseViewModel
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


class VideoListViewModel(private val videosUseCase: VideosUseCase) : BaseViewModel() {

    private val videoList = MutableLiveData<List<Video>>()

    val observableVideoList: LiveData<List<Video>>
        get() = videoList

    fun load() {
        disposables += videosUseCase.execute()
            .subscribeOn(Schedulers.io())
            .subscribeBy(onSuccess = videoList::postValue, onError = Timber::e)
    }


    // UTILS
    // -----
    private fun pagedListConfig() = PagedList.Config.Builder()
        .setInitialLoadSizeHint(5)
        .setEnablePlaceholders(false)
        .setPageSize(5 * 2)
        .build()
}