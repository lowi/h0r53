package io.obacht.h0r53.presentation.user.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import xyz.lowi.h0r53.databinding.FragmentLoginBinding


class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding

    // lazy inject BusinessService into property
//    private val authenticationViewModel: AuthenticationViewModel by inject()

//    private val authenticationViewModel by viewModel<AuthenticationViewModel> { parametersOf(this.context) }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentLoginBinding>(
            inflater,
            xyz.lowi.h0r53.R.layout.fragment_login,
            container,
            false
        )

        binding.lifecycleOwner = this
//        binding.delegate = AuthenticationScreenDelegate(authenticationViewModel)

//        authenticationViewModel.c.observe(this, Observer {
//            it?.let {
//                initRecyclerView(it)
//            }
//        })


        return binding.root


//        val userViewModel = UserViewModel()
//        userViewModel.setUser(User("王浩", "12345678912", 24, true))
//        binding.setFragmentUserViewModel(userViewModel)
//
//
//        DataBindingUtil.setContentView<FragmentLoginBinding>(activity, xyz.lowi.h0r53.R.layout.fragment_login).also {
//            it.setLifecycleOwner(this)
//            it.delegate = QueryScreenDelegate(authenticationViewModel, searchViewModel, QueryVolumeAdapter(), navigator)
//        }
//
//
//
//        binding = FragmentLoginBinding.inflate(inflater, container, false)
//            .apply {
//                vm = authenticationViewModel
//            }
//        authenticationViewModel.connect()

    }

}
