package io.obacht.h0r53.presentation.di

import android.app.Application
import arrow.integrations.retrofit.adapter.CallKindAdapterFactory
import arrow.syntax.function.pipe
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import xyz.lowi.h0r53.BuildConfig

private const val baseUrl = "https://api.github.com/"

private fun logInterceptor() =
    HttpLoggingInterceptor().apply { this.level = HttpLoggingInterceptor.Level.BODY }

private var retrofitOption: Retrofit? = null

private fun provideHttpCache(application: Application): Cache {
    val cacheSize = 10 * 1024 * 1024
    return Cache(application.cacheDir, cacheSize.toLong())
}

private fun httpClientBuilder(): OkHttpClient.Builder =
    OkHttpClient.Builder().apply {
        if (BuildConfig.DEBUG) {
            this.addInterceptor(logInterceptor())
        }
    }

private fun getRetrofitBuilderDefaults() =
    Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CallKindAdapterFactory.create())

private fun provideOkHttpClientOAuth(cache: Cache): OkHttpClient =
    httpClientBuilder().cache(cache).build()

private fun createRetrofit(application: Application): Retrofit =
    provideHttpCache(application) pipe { cache ->
        provideOkHttpClientOAuth(cache)
    } pipe { httpClient ->
        getRetrofitBuilderDefaults().client(httpClient).build()
    }

fun initRetrofit(application: Application) {
    if (retrofitOption == null) {
        retrofitOption =
            createRetrofit(application)
    }
}

fun retrofit(): Retrofit =
    retrofitOption ?: throw Exception("You need to initialize retrofit before using it!")
