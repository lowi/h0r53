package io.obacht.h0r53.presentation.platform

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso


fun View.gone(gone: Boolean) = with(this) {
    visibility = when (gone) {
        true -> View.GONE
        false -> View.VISIBLE
    }
}


@BindingAdapter("image")
fun bindImage(image: ImageView, url: String) = Picasso.with(image.context).load(url).into(image)