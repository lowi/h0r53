package io.obacht.h0r53.presentation.platform

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView
import xyz.lowi.h0r53.BR


abstract class BaseAdapter :
//    PagedListAdapter<Video, VideoListAdapter.ViewHolder>(diffCallback),
    RecyclerView.Adapter<BaseAdapter.ViewHolder>(),
    FastScrollRecyclerView.SectionedAdapter {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater, viewType, parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val obj = getObjForPosition(position)
        holder.bind(obj)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    protected abstract fun getObjForPosition(position: Int): Any

    protected abstract fun getLayoutIdForPosition(position: Int): Int


    class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: Any) {
            binding.setVariable(BR.obj, obj)
            // The executePendingBindings() is important!
            // It forces the bindings to run immediately instead of delaying them until the next frame.
            binding.executePendingBindings()
        }
    }

}

abstract class SingleLayoutAdapter(private val layoutId: Int) : BaseAdapter() {

    override fun getLayoutIdForPosition(position: Int): Int {
        return layoutId
    }
}
