package io.obacht.h0r53.presentation.di

import io.obacht.h0r53.data.source.ApiClient
import io.obacht.h0r53.data.source.ConnectionServiceImpl
import io.obacht.h0r53.domain.services.ConnectionService
import io.obacht.h0r53.domain.usecases.ConnectUseCase
import io.obacht.h0r53.domain.usecases.VideosUseCase
import io.obacht.h0r53.presentation.ClientTokenViewModel
import io.obacht.h0r53.presentation.video.VideoListViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit


//private const val BASE_URL = "https://peertube.lowi.xyz/api/v1/"
private const val BASE_URL = "http://192.168.1.68:8080/"
private const val RETROFIT_INSTANCE = "Retrofit"
private const val RETROFIT_API = "Api"
//private const val USER_REPOSITORY = "userRepository"
//private const val AUTHENTICATION_USE_CASE = "authenticationUseCase"


val appModule = module {

    // Network
    single(named(RETROFIT_INSTANCE)) { createNetworkClient(BASE_URL) }
    single<ApiClient>(named(RETROFIT_API)) {
        (get(named(RETROFIT_INSTANCE)) as Retrofit).create(ApiClient::class.java)
    }

    single<ConnectionService> {
        ConnectionServiceImpl(get(named(RETROFIT_API)))
    }

//    // Repositories
//    single<UserRepository>(named(USER_REPOSITORY)) {
//        PeertubeUserRepository(get(named(RETROFIT_API)))
//    }

    // Use cases
    factory {
        ConnectUseCase(
            connectionService = get(),
            threadExecutor = Schedulers.io(),
            postExecutionThread = AndroidSchedulers.mainThread()
        )
    }

    factory {
        VideosUseCase(
            connectionService = get(),
            threadExecutor = Schedulers.io(),
            postExecutionThread = AndroidSchedulers.mainThread()
        )
    }

//    factory { SuggestionViewModel(get(parameters = { it })) }
//    factory { AuthenticationViewModel(named(AUTHENTICATION_USE_CASE)), get(parameters = { it })) }

    viewModel {
        ClientTokenViewModel(get())
    }

    viewModel {
        VideoListViewModel(get())
    }
}
