package io.obacht.h0r53.presentation.platform

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import xyz.lowi.h0r53.R

fun <A, B> left(a: A): Either<A, B> = a.left()
fun <A, B> right(b: B): Either<A, B> = b.right()

operator fun CompositeDisposable.plus(disposable: Disposable): CompositeDisposable = apply {
    add(disposable)
}


val rootDestinations = setOf(R.id.home_dest, R.id.library_dest, R.id.settings_dest)
