package io.obacht.h0r53.presentation
//
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.recyclerview.widget.RecyclerView
//import xyz.lowi.h0r53.R
//
//
//class RepositoryCardAdapter(
//    var characters: List<RepositoryViewModel> = ArrayList(),
//    val itemClick: (RepositoryViewModel) -> Unit) : RecyclerView.Adapter<RepositoryCardAdapter.ViewHolder>() {
//
//    override fun onCreateViewHolder(parent: ViewGroup, pos: Int): ViewHolder {
//        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_repository, parent, false)
//        return ViewHolder(view, itemClick)
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
//        holder.bind(characters[pos])
//    }
//
//    override fun getItemCount() = characters.size
//
//    class ViewHolder(view: View,
//                     val itemClick: (RepositoryViewModel) -> Unit) : RecyclerView.ViewHolder(
//        view) {
//
//        fun bind(hero: RepositoryViewModel) {
//            with(hero) {
////                com.squareup.picasso.Picasso.with(itemView.context).load(photoUrl).into(itemView.picture)
//                itemView.setOnClickListener { itemClick(this) }
//            }
//        }
//    }
//}
