package io.obacht.h0r53.presentation.video


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import io.obacht.h0r53.domain.entities.Video
import kotlinx.android.synthetic.main.fragment_videos.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import xyz.lowi.h0r53.R
import xyz.lowi.h0r53.databinding.FragmentVideosBinding


class VideosListFragment : Fragment() {

    private val listViewModel: VideoListViewModel by viewModel()
    private val adapter: VideoListAdapter = VideoListAdapter()
    private val clickListener: ClickListener = this::clicked



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentVideosBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_videos,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        listViewModel.observableVideoList.observe(this, Observer { videos ->
            videos?.let { render(videos) }
        })
        listViewModel.load()
    }


    private fun render(videoList: List<Video>) {
        adapter.updateVideos(videoList)
        if (videoList.isEmpty()) {
            videoRecyclerView.visibility = View.GONE
            notesNotFound.visibility = View.VISIBLE
        } else {
            videoRecyclerView.visibility = View.VISIBLE
            notesNotFound.visibility = View.GONE
        }
    }

    private fun clicked(video: Video) {
        Timber.d("video clicked: $video")
//        val navDirections = actionNotesToNoteDetail(note.id)
//        view?.let {
//            Navigation.findNavController(it).navigate(navDirections)
//        }
    }

    private fun setupRecyclerView() {
        videoRecyclerView.layoutManager = LinearLayoutManager(this.context)
        videoRecyclerView.adapter = adapter
    }

}
