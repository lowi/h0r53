package io.obacht.h0r53.presentation.modules.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import io.obacht.h0r53.domain.entities.Video
import io.obacht.h0r53.presentation.video.VideoListAdapter
import io.obacht.h0r53.presentation.video.VideoListViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_videos.*
import kotlinx.android.synthetic.main.fragment_videos.videoRecyclerView
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import xyz.lowi.h0r53.R
import xyz.lowi.h0r53.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private val listViewModel: VideoListViewModel by viewModel()
    private val adapter: VideoListAdapter = VideoListAdapter()
    private lateinit var binding: FragmentHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbar_home)
        toolbar.inflateMenu(R.menu.menu_home)
        toolbar.setOnMenuItemClickListener(this::onOptionsItemSelected)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView(binding)

        listViewModel.observableVideoList.observe(this, Observer { videos ->
            Timber.d("video list: $videos")
            videos?.let { render(videos) }
        })
        listViewModel.load()
    }

    private fun setupRecyclerView(binding: FragmentHomeBinding) {
        binding.videoRecyclerView.layoutManager = LinearLayoutManager(this.context)
        binding.videoRecyclerView.adapter = adapter
    }

    private fun render(videoList: List<Video>) {
        adapter.updateVideos(videoList)
        if (videoList.isEmpty()) {
            videoRecyclerView.visibility = View.GONE
            videosNotFound.visibility = View.VISIBLE
        } else {
            videoRecyclerView.visibility = View.VISIBLE
            videosNotFound.visibility = View.GONE
        }
    }

}
