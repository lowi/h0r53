package io.obacht.h0r53.domain.services

import arrow.core.Either
import io.obacht.h0r53.domain.entities.AccessToken
import io.obacht.h0r53.domain.entities.ClientToken
import io.obacht.h0r53.domain.entities.Credentials
import io.reactivex.Maybe
import io.reactivex.Single

interface UserService {
    fun getClientToken(): Single<Either<Error, ClientToken>>
    fun setClientToken(token: ClientToken): Maybe<Error>

    fun getAccessToken(): Single<Either<Error, AccessToken>>
    fun setAccessToken(token: AccessToken): Maybe<Error>

    fun getCredentials(): Single<Either<Error, Credentials>>
    fun setCredentials(credentials: Credentials): Maybe<Error>

    sealed class Error {
        object TokenNotFound : Error()
        object CantSaveToken : Error()
        object CredentialsNotFound : Error()
        object CantSaveCredentials : Error()
    }
}
