package io.obacht.h0r53.domain.usecases

import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


/**
 * Here we define all our abstract use cases.
 *
 * Use case composed from:
 *  ⚫ Reactive type - (Observable/Flowable/Single/Maybe/Completable)
 *  ⚫ Data (Optional) - The data which use case will emit
 *  ⚫ Error (Optional) - Expected use case error and will be sealed class
 *  ⚫ Parameter (Optional)
 *
 */


abstract class UseCase(
    private val threadExecutor: Scheduler,
    private val postExecutionThread: Scheduler
) {

    private var lastDisposable: Disposable? = null
    private val compositeDisposable = CompositeDisposable()

    fun disposeLast() {
        lastDisposable?.let {
            if (!it.isDisposed) {
                it.dispose()
            }
        }
    }

    fun dispose() {
        compositeDisposable.clear()
    }
}


abstract class UseCaseWithoutParam<out T>(
    threadExecutor: Scheduler,
    postExecutionThread: Scheduler
) : UseCase(threadExecutor, postExecutionThread) {

    abstract fun build(): T

    abstract fun execute(): T
}


abstract class UseCaseWithParam<out T, in P>(
    threadExecutor: Scheduler,
    postExecutionThread: Scheduler
) : UseCase(threadExecutor, postExecutionThread) {

    abstract fun build(param: P): T

    abstract fun execute(param: P): T
}


abstract class CompletableWithParamUseCase<in P>(
    threadExecutor: Scheduler,
    postExecutionThread: Scheduler
) : UseCaseWithParam<Completable, P>(
    threadExecutor,
    postExecutionThread
)


abstract class CompletableUseCase(threadExecutor: Scheduler, postExecutionThread: Scheduler) :
    UseCaseWithoutParam<Completable>(
        threadExecutor,
        postExecutionThread
    )


abstract class ObservableWithParamUseCase<T, in P>(
    private val threadExecutor: Scheduler,
    private val postExecutionThread: Scheduler
) : UseCaseWithParam<Observable<T>, P>(
    threadExecutor,
    postExecutionThread
) {
    override fun execute(param: P): Observable<T> {
        return build(param)
            .subscribeOn(threadExecutor)
            .observeOn(postExecutionThread)
    }
}


abstract class ObservableWithoutParamUseCase<T>(
    private val threadExecutor: Scheduler,
    private val postExecutionThread: Scheduler
) : UseCaseWithoutParam<Observable<T>>(
    threadExecutor,
    postExecutionThread
) {
    override fun execute(): Observable<T> {
        return build()
            .subscribeOn(threadExecutor)
            .observeOn(postExecutionThread)
    }
}


abstract class SingleWithParamUseCase<T, in P>(
    private val threadExecutor: Scheduler,
    private val postExecutionThread: Scheduler
) : UseCaseWithParam<Single<T>, P>(
    threadExecutor,
    postExecutionThread
) {
    override fun execute(param: P): Single<T> {
        return build(param)
            .subscribeOn(threadExecutor)
            .observeOn(postExecutionThread)
    }
}


abstract class SingleUseCase<T>(
    private val threadExecutor: Scheduler,
    private val postExecutionThread: Scheduler
) : UseCaseWithoutParam<Single<T>>(
    threadExecutor,
    postExecutionThread
) {
    override fun execute(): Single<T> {
        return build()
            .subscribeOn(threadExecutor)
            .observeOn(postExecutionThread)
    }
}


abstract class MaybeWithParamUseCase<T, in P>(
    private val threadExecutor: Scheduler,
    private val postExecutionThread: Scheduler
) : UseCaseWithParam<Maybe<T>, P>(
    threadExecutor,
    postExecutionThread
) {

    override fun execute(param: P): Maybe<T> {
        return build(param)
            .subscribeOn(threadExecutor)
            .observeOn(postExecutionThread)
    }
}


abstract class MaybeUseCase<T>(
    private val threadExecutor: Scheduler,
    private val postExecutionThread: Scheduler
) : UseCaseWithoutParam<Maybe<T>>(
    threadExecutor,
    postExecutionThread
) {
    override fun execute(): Maybe<T> =
        build()
            .subscribeOn(threadExecutor)
            .observeOn(postExecutionThread)

}


abstract class FlowableWithParamUseCase<T, in P>(
    private val threadExecutor: Scheduler,
    private val postExecutionThread: Scheduler
) : UseCaseWithParam<Flowable<T>, P>(
    threadExecutor,
    postExecutionThread
) {

    override fun execute(param: P): Flowable<T> {
        return build(param)
            .subscribeOn(threadExecutor)
            .observeOn(postExecutionThread)
    }
}


abstract class FlowableUseCase<StreamT>(
    private val threadExecutor: Scheduler,
    private val postExecutionThread: Scheduler
) :
    UseCaseWithoutParam<Flowable<StreamT>>(
        threadExecutor,
        postExecutionThread
    ) {
    override fun execute(): Flowable<StreamT> =
        build()
            .subscribeOn(threadExecutor)
            .observeOn(postExecutionThread)

}
