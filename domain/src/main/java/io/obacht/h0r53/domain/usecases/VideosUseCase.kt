package io.obacht.h0r53.domain.usecases


import io.obacht.h0r53.domain.entities.Video
import io.obacht.h0r53.domain.services.ConnectionService
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.Single


class VideosUseCase(
    private val connectionService: ConnectionService,
    threadExecutor: Scheduler,
    postExecutionThread: Scheduler
) : SingleUseCase<List<Video>>(
    threadExecutor,
    postExecutionThread
) {
    override fun build(): Single<List<Video>> =
        connectionService
            .videos()

//    PageKeyedDataSource<Int, User>


    sealed class Error {
        object ConnectionFailed : Error()
    }
}


