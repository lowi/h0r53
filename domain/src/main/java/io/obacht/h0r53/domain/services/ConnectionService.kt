package io.obacht.h0r53.domain.services

import io.obacht.h0r53.domain.entities.ClientToken
import io.obacht.h0r53.domain.entities.Video
import io.reactivex.Single


interface ConnectionService {
    fun connect(): Single<ClientToken>
    fun videos(): Single<List<Video>>


    sealed class Error {
        object NetworkError : Error()
    }

}

