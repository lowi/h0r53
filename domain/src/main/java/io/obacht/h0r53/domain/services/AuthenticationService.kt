package io.obacht.h0r53.domain.services

import arrow.core.Either
import io.obacht.h0r53.domain.entities.AccessToken
import io.obacht.h0r53.domain.entities.ClientToken
import io.obacht.h0r53.domain.entities.Credentials
import io.reactivex.Maybe
import io.reactivex.Single


interface AuthenticationService {
    fun login(loginData: Credentials): Single<Either<Error, AccessToken>>
    fun logOut(token: AccessToken): Maybe<Error>


    sealed class Error {
        object NetworkError : Error()
        object EmailNotExist : Error()
        object WrongPassword : Error()
    }

}

