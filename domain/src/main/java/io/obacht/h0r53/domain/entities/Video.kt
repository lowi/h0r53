package io.obacht.h0r53.domain.entities

data class VideoList(
    val total: Int,
    val data: List<Video>
)

data class Video(
    val id: Int,
    val uuid: String,
    val category: Category,
    val licence: Licence,
    val language: Language,
    val privacy: Privacy,
    val nsfw: Boolean = false,
    val description: String,
    val isLocal: Boolean,
    val duration: Number,
    val views: Int = 0,
    val likes: Int = 0,
    val dislikes: Int = 0,
    val thumbnailPath: String,
    val previewPath: String,
    val embedPath: String,
    val createdAt: String,
    val updatedAt: String,
    val publishedAt: String,
    val originallyPublishedAt: String,
    val account: Account,
    val channel: Channel
)


data class Category(val id: Number, val label: String)
data class Licence(val id: Number, val label: String)
data class Language(val id: Number, val label: String)
data class Privacy(val id: Number, val label: String)


data class Account(
    val id: Number,
    val uuid: String,
    val name: String,
    val displayName: String,
    val url: String,
    val avatar: String
)

data class Channel(
    val id: Number,
    val uuid: String,
    val name: String,
    val displayName: String,
    val url: String,
    val host: String,
    val avatar: String
)