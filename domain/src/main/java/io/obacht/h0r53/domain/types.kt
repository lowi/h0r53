package io.obacht.h0r53.domain

import arrow.core.Either
import arrow.core.NonEmptyList

typealias ErrorOr<A> = Either<NonEmptyList<String>, A>