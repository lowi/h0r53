package io.obacht.h0r53.domain.entities

import com.google.gson.annotations.SerializedName


data class ClientToken(
    @SerializedName("client_id")
    val id: String,
    @SerializedName("client_secret")
    val secret: String
)

data class Credentials(
    @SerializedName("client_id")
    val clientId: String,
    @SerializedName("client_secret")
    val clientSecret: String,
    @SerializedName("grant_type")
    val grantType: String = "password",
    @SerializedName("response_type")
    val responseType: String = "code",
    val username: String,
    val password: String
)

data class AccessToken(
    @SerializedName("access_token")
    val token: String,
    @SerializedName("token_type")
    val type: String,
    @SerializedName("expires_in")
    val expiresIn: Int,
    @SerializedName("refresh_token")
    val refreshToken: String
)

data class User(
    val id: Long,
    @SerializedName("avatar_url") val avatarUrl: String,
    val login: String
)


data class Repository(
    val id: Long,
    val name: String,
    val description: String,
    val owner: User,
    @SerializedName("forks_count") val forksCount: Int,
    @SerializedName("stargazers_count") val stargazersCount: Int
)

