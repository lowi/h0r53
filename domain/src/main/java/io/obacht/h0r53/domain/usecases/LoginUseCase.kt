package io.obacht.h0r53.domain.usecases

import io.obacht.h0r53.domain.entities.AccessToken
import io.obacht.h0r53.domain.entities.Credentials
import io.obacht.h0r53.domain.services.AuthenticationService
import io.obacht.h0r53.domain.services.UserService
import io.obacht.h0r53.domain.services.ValidationService
import io.reactivex.Maybe
import io.reactivex.Scheduler


class LoginUseCase(
    private val authenticationService: AuthenticationService,
    private val validationService: ValidationService,
    private val userService: UserService,
    threadExecutor: Scheduler,
    postExecutionThread: Scheduler
) : MaybeWithParamUseCase<LoginUseCase.Error, Credentials>(
    threadExecutor,
    postExecutionThread
) {

    override fun build(credentials: Credentials)
            : Maybe<Error> {
        return with(credentials) {
            Maybe.concat(
//                validationService.validateEmail(email)
//                    .map { (Error.InvalidEmail) },

                validationService.validatePassword(password)
                    .map { (Error.InvalidPassword) },

                authenticationService
                    .login(credentials)
                    .flatMapMaybe {
                        it.fold(
                            { error ->
                                Maybe.just(
                                    when (error) {
                                        AuthenticationService.Error.EmailNotExist -> Error.EmailNotExist
                                        AuthenticationService.Error.WrongPassword -> Error.WrongPassword
                                        AuthenticationService.Error.NetworkError -> Error.LoginFailed
                                    }
                                )
                            },
                            { token -> setToken(userService, token) }
                        )
                    }
            ).firstElement()
        }
    }

    private fun setToken(userService: UserService, token: AccessToken)
            : Maybe<Error> {
        return userService.setAccessToken(token)
            .map { Error.LoginFailed }
    }

    sealed class Error {
        object InvalidEmail : Error()
        object InvalidPassword : Error()
        object EmailNotExist : Error()
        object WrongPassword : Error()
        object LoginFailed : Error()
    }

}


