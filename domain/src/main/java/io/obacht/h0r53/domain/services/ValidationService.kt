package io.obacht.h0r53.domain.services

import io.reactivex.Maybe

interface ValidationService {
    fun validateEmail(email: String): Maybe<Error>
    fun validatePassword(password: String): Maybe<Error>

    sealed class Error {
        sealed class InvalidEmail
        sealed class InvalidPassword
    }
}

