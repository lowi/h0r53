package io.obacht.h0r53.domain.user

import arrow.core.Option
import io.obacht.h0r53.domain.entities.ClientToken
import io.obacht.h0r53.domain.entities.Credentials
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single


interface UserDataSource<T> {
    fun connect(): Single<Option<ClientToken>>
    fun login(credentials: Credentials): Flowable<List<T>>
    fun logout(): Completable
}


interface UserRepository {
    fun connect(): Single<ClientToken>
}


//
//class DeferUserRepository<F>(
//    private val monadDefer: MonadDefer<F>
//) : UserRepository<F> {
//
//    override fun connect(): Kind<F, ClientToken> = monadDefer.later {
//        store.invoke {
//            select(Employee::class)
//                .get()
//                .toList()
//        }
//    }
//
//}