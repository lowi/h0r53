package io.obacht.h0r53.domain.usecases


import io.obacht.h0r53.domain.entities.ClientToken
import io.obacht.h0r53.domain.services.ConnectionService
import io.reactivex.Scheduler
import io.reactivex.Single


class ConnectUseCase(
    private val connectionService: ConnectionService,
    threadExecutor: Scheduler,
    postExecutionThread: Scheduler
) : SingleUseCase<ClientToken>(
    threadExecutor,
    postExecutionThread
) {

    override fun build(): Single<ClientToken> =
        connectionService
            .connect()


    sealed class Error {
        object ConnectionFailed : Error()
    }
}


